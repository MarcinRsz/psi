class Hebb {

    //główna metoda reprezentująca i wywołująca deltę
    public static void main(final String args[]) {
        final Hebb delta = new Hebb();
        delta.run();

    }

    // waga neuronu 1
    double w1;

    // waga neuronu 2
    double w2;

    // stopien nauki
    double rate = 1.0;

    // bieżący poziom nauki
    int epoch = 1;

    public Hebb() {
        this.w1 = 1;
        this.w2 = -1;
    }

    // Tworzymy pierwszy trening. Nauczymy siec przykładów, a później zmienimy ich wagi oparte na błędach
    protected void epoch() {
        System.out.println("***Beginning Epoch #" + this.epoch + "***");
        presentPattern(-1, -1);
        presentPattern(-1, 1);
        presentPattern(1, -1);
        presentPattern(1, 1);
        this.epoch++;
    }

    //Pokazanie wzorcu i nauka na nim
    protected void presentPattern(final double i1, final double i2) {
        double result;
        double delta;

        // uruchom siec podczas treningu
        // otrzymanie błędu
        System.out.print("Presented [" + i1 + "," + i2 + "]");
        result = recognize(i1, i2);
        System.out.print(" result=" + result);

        // ustawienie wagi neuronu 1
        delta = trainingFunction(this.rate, i1, result);
        this.w1 += delta;
        System.out.print(",delta w1=" + delta);

        // ustawienie wagi neuronu 2
        delta = trainingFunction(this.rate, i2, result);
        this.w2 += delta;
       // System.out.println("Patrz na wage druga:"+ w2);
        System.out.println(",delta w2=" + delta);

    }

    protected double recognize(final double i1, final double i2) {
        final double a = (this.w1 * i1) + (this.w2 * i2);
        return (a * .5);
    }

    // utworzenie powtórzeń nauki (dla naszego przypadku 5 razy)
    public void run() {
        for (int i = 0; i < 5; i++) {
            epoch();
        }
    }

    // reguła delty
    protected double trainingFunction(final double rate, final double input,
                                      final double output) {
        return rate * input * output;
    }
}