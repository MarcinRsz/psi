# Projekt- Podstawy Sztucznej Inteligencji


## Getting Started

Repozytorium zostało stworzone w celu dodawania zadań z przedmiotu podstawy sztucznej inteligencji.

## List of algorithms

* Metoda Kohonena
* Metoda Hopfielda
* Reguła Hebbiana
* Problem XOR
* Sieć neuronowa

## Authors

M.Szczygieł